#ifndef READER_H
#define READER_H

#include "../utils/bmh_header.h"
#include "../utils/bmp_header_conf.h"
#include "../utils/image.h"

#include <errno.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>


struct image;


enum read_status  {
  READ_OK,
  READ_INVALID_CANNOT_OPEN_FILE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  CREATE_INVALID_MEMORY_PROBLEMS
  /* коды других ошибок  */
  };

enum read_status read(char *file_path, struct image *img);

enum read_status from_bmp( FILE* in, struct image *img);




#endif

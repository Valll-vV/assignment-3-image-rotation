#include "reader.h"

enum read_status read(char *file_path, struct image *img){
    FILE *in = fopen(file_path, "r");
    if (in == 0){
        return READ_INVALID_CANNOT_OPEN_FILE;
    }
    enum read_status flag  = from_bmp(in, img);
    fclose(in);
    printf("%s \n", strerror(errno));
    return flag;
}

enum read_status from_bmp(FILE *in, struct image *img)
{

    struct bmp_header bmp_header;

    if (fread(&bmp_header, sizeof(struct bmp_header), 1, in) != 1){
        return READ_INVALID_HEADER;
    }

    uint64_t width = bmp_header.biWidth;
    uint64_t height = bmp_header.biHeight;
    uint8_t pd = find_padding(width);

    if (bmp_header.bfType != BF_TYPE){
        return READ_INVALID_HEADER;
    }

    if (bmp_header.biBitCount != BI_BIT_COUNT){
        return READ_INVALID_HEADER;
    }

    if (bmp_header.biPlanes != BI_PLANES){
        return READ_INVALID_HEADER;
    }

    if (bmp_header.biCompression != BI_COMPRESSION){
        return READ_INVALID_HEADER;
    }

    if (fseek(in, bmp_header.bOffBits, SEEK_SET) != 0){
        return READ_INVALID_HEADER;
    }
    
    if (create_image(img, width, height)){
        return CREATE_INVALID_MEMORY_PROBLEMS;
    }    

    struct pixel *pic = img->data;
    uint32_t one_line = width * 3 + pd;

    for (uint64_t a = 0; a < height; a++){
        if (fread(pic, 1, one_line, in) - one_line != 0){
            return READ_INVALID_BITS;
        }
        else {
            pic += width;
        }
    }

    return READ_OK;
}

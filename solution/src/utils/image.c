#include "image.h"

int create_image(struct image *img,  uint64_t width, uint64_t height){

    img->width = width;    
    img->height = height;  
    img->data = malloc(width* height * (sizeof(struct pixel))+4);

    if (img->data == 0){
        return 1;
    }
    return 0;
}

void clear_image(struct image *img){
    free(img -> data);
}

uint8_t find_padding(uint64_t width){
    uint16_t temp = (width * 3) % 4;
    if (temp == 0) {
        temp = 4;
    }
    uint16_t pd = 4 - temp;
    return pd;
}

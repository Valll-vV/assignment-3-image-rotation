#ifndef IMAGE_H
#define IMAGE_H

#include "../read/reader.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct pixel { 
    uint8_t b, g, r; 
};

uint8_t find_padding(uint64_t width);

int create_image(struct image* img, uint64_t width, uint64_t height);

void clear_image(struct image* img);



#endif

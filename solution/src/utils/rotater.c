#include "rotater.h"

int rotate(struct image *img){

    struct image rotated;

    if(create_image(&rotated, img->height, img->width)){
        return 1;
    }

    for(int a = 0; a < img->width; a++){
        for(int b = 0; b < img->height; b++){
            rotated.data[img->height - b - 1 + rotated.width * a] = img->data[a + img->width * b];
        }
    }

    clear_image(img);
    *img = rotated;

    return 0;

}

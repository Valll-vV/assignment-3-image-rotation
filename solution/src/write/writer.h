#ifndef WRITER_H
#define WRITER_H


#include "../utils/bmh_header.h"
#include "../utils/bmp_header_conf.h"
#include "../utils/image.h"

#include <errno.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>


enum  write_status  {
  WRITE_OK,
  WRITE_INVALID_CANNOT_WRITE_HEADER,
  WRITE_INVALID_CANNOT_OPEN_FILE,
  WRITE_INVALID_CANNOT_WRITE_PICTURE_BITS
};

enum write_status to_bmp( FILE* out, struct image* img);

enum write_status write(char *file_path, struct image *img);


#endif

#include "writer.h"

enum write_status write(char *file_path, struct image *img){
    FILE *out = fopen(file_path, "w");
    if (out == 0){
        printf("cannot open file to write \n");
        return WRITE_INVALID_CANNOT_OPEN_FILE;
    }
    enum write_status flag = to_bmp(out, img);
    fclose(out);
    printf("%s \n", strerror(errno));
    return flag;
}

enum write_status to_bmp(FILE *out, struct image *img)
{
    struct bmp_header bmp_header = {0};

    bmp_header.bfType = BF_TYPE;
    uint8_t pd = find_padding(img->width);
    bmp_header.bfileSize = sizeof(struct bmp_header) + (img->width * 3 + pd) * img->height;
    bmp_header.bfileSize = BFILE_SIZE;
    bmp_header.bOffBits = sizeof(struct bmp_header);
    bmp_header.biSize = BI_SIZE;
    bmp_header.biWidth = img->width;
    bmp_header.biHeight = img->height;
    bmp_header.biPlanes = BI_PLANES;
    bmp_header.biBitCount = 24;
    bmp_header.biCompression = BI_COMPRESSION;
    bmp_header.biSizeImage = BI_SIZE_IMAGE;
    bmp_header.biXPelsPerMeter = BI_X_PELS_PER_METER;
    bmp_header.biYPelsPerMeter = BI_Y_PELS_PER_METER;
    bmp_header.biClrUsed = BI_CLR_USED;
    bmp_header.biClrImportant = BI_CLR_IMPORTANT;

    if (fwrite(&bmp_header, sizeof(struct bmp_header), 1, out) != 1){
        return WRITE_INVALID_CANNOT_WRITE_HEADER;
    }

    void *temp = img->data;


    uint64_t one_line = img->width * 3 + pd;

    for (uint64_t a = 0; a < img->height -1; a++){
        if (fwrite(img->data, 1, one_line, out) - one_line != 0) return WRITE_INVALID_CANNOT_WRITE_PICTURE_BITS;
        else {
            img->data += img->width;
        }
    }

    one_line -= pd;

    if (fwrite(img->data, 1, one_line, out) - one_line != 0){
        return WRITE_INVALID_CANNOT_WRITE_PICTURE_BITS;
    }

    img->data = temp;

    return WRITE_OK;
}


